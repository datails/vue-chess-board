import { createStore } from "vuex";
import { SET_ACTIVE_FIELD } from "./mutation-types";

const store = createStore({
  state: {
    board: [
      ["c", "r", "b", "q", "k", "b", "r", "c"],
      ["p", "p", "p", "p", "p", "p", "p", "p"],
      ["", "", "", "", "", "", "", ""],
      ["", "", "", "", "", "", "", ""],
      ["", "", "", "", "", "", "", ""],
      ["", "", "", "", "", "", "", ""],
      ["P", "P", "P", "P", "P", "P", "P", "P"],
      ["C", "R", "B", "Q", "K", "B", "R", "C"],
    ],
    activeField: null,
  },
  actions: {
    setActiveField ({ commit }, field) {
      commit(SET_ACTIVE_FIELD, field)
    }
  },
  mutations: {
    [SET_ACTIVE_FIELD] (state, newField) {
      state.activeField = newField;
    },
  },
  getters: {
    activeField: ({ activeField }) => activeField,
    board: ({ board }) => board,
  }
});

export default store;
